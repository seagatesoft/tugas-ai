if '__main__' == __name__:
    td = TrainingData()
    td.add(
        'http://www.imdb.com/title/tt2488496',
        [
            'Star Wars: The Force Awakens',
            'J.J. Abrams',
        ],
    )
    td.add(
        'http://www.imdb.com/title/tt2975590',
        [
            'Batman v Superman: Dawn of Justice',
            'Zack Snyder',
        ],
    )
    td.add(
        'http://www.imdb.com/title/tt3385516/',
        [
            'X-Men: Apocalypse',
            'Bryan Singer',
        ],
    )
    trainer = Trainer()
    scraper = trainer.train(td)

    test_data = [
        'http://www.imdb.com/title/tt1386697/',
        'http://www.imdb.com/title/tt2660888/',
        'http://www.imdb.com/title/tt2381991/',
        'http://www.imdb.com/title/tt3797868/',
        'http://www.imdb.com/title/tt3496992/',
        'http://www.imdb.com/title/tt3300542/',
        'http://www.imdb.com/title/tt3748528/',
    ]

    for test_url in test_data:
        print scraper.scrape(test_url)
