import logging
import operator
import re

from lxml import html

logging.basicConfig(filename='scraper.log', level=logging.DEBUG)


class TrainingData(object):
    def __init__(self):
        self.samples = []

    def add(self, url, expected_texts):
        self.samples.append((url, expected_texts))


class Trainer(object):
    def train(self, training_data, absolute_xpath=True):
        texts_xpaths_matching = self._match_texts_and_xpaths(training_data, absolute_xpath)
        logging.debug('texts_xpaths_matching = %s', texts_xpaths_matching)
        best_xpaths = self._get_best_xpaths(texts_xpaths_matching)
        logging.debug('best_xpaths = %s', best_xpaths)

        return Scraper(best_xpaths)

    def calculate_text_similarity(self, s1, s2):
        m = [[0] * (1 + len(s2)) for i in xrange(1 + len(s1))]
        longest, x_longest = 0, 0

        for x in xrange(1, 1 + len(s1)):
            for y in xrange(1, 1 + len(s2)):
                if s1[x - 1] == s2[y - 1]:
                    m[x][y] = m[x - 1][y - 1] + 1
                    if m[x][y] > longest:
                        longest = m[x][y]
                        x_longest = x
                else:
                    m[x][y] = 0

        lcs = s1[x_longest - longest: x_longest]

        return float(len(lcs)) / float(max(len(s1), len(s2)))

    def sanitize_text(self, text):
        modified_text = re.sub(r'[\r\n\t]', ' ', text)
        modified_text = re.sub(r'\s+', '', modified_text)
        return modified_text

    def _match_texts_and_xpaths(self, training_data, absolute_xpath):
        texts_xpaths_matching = {}
        for sample in training_data.samples:
            url, expected_texts = sample
            html_tree = html.parse(url)

            for index, expected_text in enumerate(expected_texts):
                elements = html_tree.xpath("//*[contains(text(), '%s')]" % expected_text)

                if not elements:
                    logging.error("Expected text '%s' not found on %s", expected_text, url)
                    continue

                for element in elements:
                    element_text = ''.join(element.xpath("./text()"))
                    text_similarity = self.calculate_text_similarity(expected_text, element_text)
                    if absolute_xpath:
                        xpath = html_tree.getpath(element)
                    else:
                        xpath = self._get_xpath(element)

                    if index not in texts_xpaths_matching:
                        texts_xpaths_matching[index] = {}

                    if xpath not in texts_xpaths_matching[index]:
                        texts_xpaths_matching[index][xpath] = []

                    texts_xpaths_matching[index][xpath].append(text_similarity)

        return texts_xpaths_matching

    def _get_best_xpaths(self, texts_xpaths_matching):
        xpaths = {}
        for index, matched_xpaths in texts_xpaths_matching.iteritems():
            xpaths_score = {}
            for xpath in matched_xpaths:
                xpaths_score[xpath] = sum(matched_xpaths[xpath])
            logging.debug('xpaths_score = %s', xpaths_score)
            best_xpath = max(xpaths_score.iteritems(), key=operator.itemgetter(1))[0]
            logging.debug('best_xpath = %s', best_xpath)
            xpaths[index] = [best_xpath]

        return xpaths

    def _get_xpath(self, element):
        paths = []
        parent = [element]
        id_found = False

        while len(parent) > 0 and not id_found:
            element_xpath = self._get_element_xpath(parent[0])
            paths.append(element_xpath)
            if 'id' in parent[0].attrib:
                id_found = True
            parent = parent[0].xpath("..")

        paths.reverse()

        return ''.join(paths)

    def _get_element_xpath(self, element):
        if 'id' in element.attrib:
            if element.attrib['id']:
                return "//*[@id='%s']" % element.attrib['id']

        if 'class' in element.attrib:
            if element.attrib['class']:
                return "/%s[@class='%s']" % (element.tag, element.attrib['class'])

        parent = element.getparent()
        if parent is None:
            return '/%s' % element.tag
        else:
            return '/%s[%s]' % (element.tag, parent.index(element) + 1)


class Scraper(object):
    def __init__(self, xpaths):
        self.xpaths = xpaths

    def scrape(self, url):
        html_tree = html.parse(url)
        data = {}

        for field in self.xpaths:
            data[field] = None
            for xpath in self.xpaths[field]:
                text = ''.join([t.strip() for t in html_tree.xpath('%s/text()' % xpath)])
                if text:
                    data[field] = text
                    break

        return data


if '__main__' == __name__:
    td = TrainingData()
    td.add(
        'http://www.gramedia.com/categories/books/fiction-literature/infernal-devices-clockwork-angel-book-one.html',
        [
            'Infernal Devices Clockwork Angel Book One',
            'Badiyanto',
            'Rp 82.790',
        ],
    )
    td.add(
        'http://www.gramedia.com/categories/books/fiction-literature/sherlock-holmes-a-collectors-edition.html',
        [
            'Sherlock Holmes A Collectors Edition',
            'Sir Arthur Conan Doyle',
            'Rp 169.150',
        ],
    )
    td.add(
        'http://www.gramedia.com/categories/books/fiction-literature/peter-pan-novel-klasik.html',
        [
            'Peter Pan - Novel Klasik',
            'J.m. Barrie',
            'Rp 32.300',
        ],
    )
    trainer = Trainer()
    scraper = trainer.train(td)

    test_data = [
        'http://www.gramedia.com/categories/books/fiction-literature/the-stories-of-king-arthurs-knights.html',
        'http://www.gramedia.com/categories/books/fiction-literature/sam-kok-epos-tiga-negara-1-roman-klasik-tiongkok.html',
        'http://www.gramedia.com/categories/books/fiction-literature/princess-bajak-laut-alien.html',
        'http://www.gramedia.com/categories/books/fiction-literature/lalu-biar-diam-menjadi-teman.html',
        'http://www.gramedia.com/categories/books/fiction-literature/lady-in-the-glass.html',
    ]

    for test_url in test_data:
        print scraper.scrape(test_url)
